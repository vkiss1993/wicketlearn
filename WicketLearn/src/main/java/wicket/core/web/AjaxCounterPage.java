package wicket.core.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;

/**
 * Ez az page a NotAjaxCounterPage-el megfelelő funkcionalitással bír, annyi különbséggel, hogy a linkre kattintva
 * egy ajax kérés megy ki a háttérbe, és nem töltődik újra az egész page. A két page között a különbséget az jelenti,
 * hogy itt nem sima Link() objektumot hozunk létre, hanem AjaxFallbackLink() objektumot, amire kattintva aszinkorn
 * történik a hívás a háttérbe.
 *
 * Az ajax Get requestre a következő response jön:
 *
 * <?xml version="1.0" encoding="UTF-8"?>
 * <ajax-response>
 * 	<component id="counter2" >
 * 		<![CDATA[<span wicket:id="counter" id="counter2">3</span>]]>
 * 	</component>
 * </ajax-response>
 *
 * Itt az jön vissza, hogy melyik komponensen változtattunk(id="counter2", és, hogy mi lett az értéke.)
 * Az érték -> <span wicket:id="counter" id="counter2">3</span>, tehát 3-ra növelte a request a számlálót
 * */
public class AjaxCounterPage extends WebPage {

    public AjaxCounterPage(){
        Model<Integer> model = new Model<Integer>() {
            private int counter = 0;
            public Integer getObject() {
                return counter++;
            }
        };
        /**
         * Itt a nem ajaxos megoldáshoz képest kiemelésre került a Label, nem pedig csak az add() hívásnál kerül
         * példányosításra. Erre azért volt szükség, hogy tudjunk az eseménykezelő kódban hivatkozni rá.
         * */
        final Label label = new Label("counter", model);
        /**
         * Ezzel azt mondjuk meg, hogy amikor a válasz vissza megy a böngészőhöz, akkor tudja frissíteni az értékét.
         * Ha ez nem lenne beállítva, akkor nem tudná a Wicket, hogyan updatelje.
         * */
        label.setOutputMarkupId(true);
        add(new AjaxFallbackLink("link") {
            public void onClick(AjaxRequestTarget target) {
                if (target != null) {
                    /**
                     * Ha azt szeretnénk, hogy egy ajax kérés hatására egy komponens frissüljön, akkor hozzá kell
                     * adni a target paraméterben kapott objektumhoz.
                     * */
                    target.add(label);
                }
            }
        });
        add(label);
    }
}
