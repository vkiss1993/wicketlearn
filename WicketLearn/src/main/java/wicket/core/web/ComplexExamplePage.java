package wicket.core.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ComplexExamplePage extends BasePage {

    private int count = 0;

    public ComplexExamplePage() {
        final Model<Integer> model = new Model<>();
        model.setObject(count);

        final Label label = new Label("counter", model);

        label.setOutputMarkupId(true);

        /**
         * <button></button> html elemre AjaxLink Objektumot hozunk létre, ezt adjuk hozzá a pagehez, így ez a gomb
         * Ajaxos lesz.
         * */
        AjaxLink btnClose = new AjaxLink("counterButton") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                model.setObject(count++);
                target.add(label);
            }
        };

        add(btnClose);
        add(label);

        RepeatingView view = new RepeatingView("LinkList");

        add(view);

        WebMarkupContainer list = new WebMarkupContainer(view.newChildId());
        ExternalLink externalLink = new ExternalLink("Link", "http://www.google.com");
        externalLink.add(new Label("Text","Google"));
        list.add(externalLink);

        view.add(list);

        WebMarkupContainer list2 = new WebMarkupContainer(view.newChildId());
        ExternalLink externalLink2 = new ExternalLink("Link", "http://www.google.com");
        externalLink2.add(new Label("Text",new StringResourceModel("MainPanel.test")));
        //externalLink2.setMarkupId("asd");
        list2.add(externalLink2);

        view.add(list);
        view.add(list2);


        /*-----------------------*/
        RepeatingView repeatingView = new RepeatingView("CockpitList");

        List<String> links = new ArrayList<>();
        links.add("ComplexExamplePage.test");
        links.add("ComplexExamplePage.other");
        links.add("ComplexExamplePage.asd");
        links.add("ComplexExamplePage.qwe");

        List<BookmarkablePageLinkDescriptor> data = new ArrayList<>();
        data.add(new BookmarkablePageLinkDescriptor("ComplexExamplePage.test", "randomId", AjaxCounterPage.class));
        data.add(new BookmarkablePageLinkDescriptor("ComplexExamplePage.other", "otherId", HelloWorld.class));
        data.add(new BookmarkablePageLinkDescriptor("ComplexExamplePage.asd", "someeeeeeeeeId", GuestBook.class));
        data.add(new BookmarkablePageLinkDescriptor("ComplexExamplePage.qwe", "finalId", AjaxCounterPage.class));

        data.stream().sorted(Comparator.comparing(e -> new StringResourceModel(e.getLanguageKey()).getString())).forEach(elem -> {
            WebMarkupContainer container = new WebMarkupContainer(repeatingView.newChildId());
            BookmarkablePageLink pageLink = new BookmarkablePageLink("MenuItem", elem.getPageClass());
            pageLink.add(new Label("WicketMessage", new StringResourceModel(elem.getLanguageKey())));
            pageLink.add(new AttributeAppender("id", elem.getAnchorId()));
            container.add(pageLink);
            repeatingView.add(container);
        });

        add(repeatingView);

        Label l = new Label("IdTest", "asd");
        l.add(AttributeModifier.append("id", "red"));
        l.add(new AttributeAppender("id", "dfas"));
        add(l);
    }

    @Data
    @AllArgsConstructor
    private class BookmarkablePageLinkDescriptor {
        private String languageKey;
        private String anchorId;
        private Class pageClass;
    }

}
