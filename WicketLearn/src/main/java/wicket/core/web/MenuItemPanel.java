package wicket.core.web;

import javafx.util.Pair;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.GenericPanel;
import org.apache.wicket.model.IModel;

public class MenuItemPanel extends GenericPanel<Pair<String, Class<? extends Page>>> {

    public MenuItemPanel(String id, IModel<Pair<String, Class<? extends Page>>> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.setOutputMarkupId(true);

        BookmarkablePageLink<? extends Page> pageLink = new BookmarkablePageLink<>("menuItem", getModelObject().getValue());
        pageLink.add(new Label("linkMessage", getModelObject().getKey()));

        add(pageLink);
    }
}
