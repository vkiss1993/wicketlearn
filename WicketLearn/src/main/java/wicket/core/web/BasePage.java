package wicket.core.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/*
* Ebből a pageből fognak leszármazni más pagek. Az látszik, hogy a két link az oldal tetején fix lesz, illetve a footer is innen jön.
* A html-ben a bodyban van definiálva az a rész, ahová a leszármazottak tetszés szerint betölthetik saját kódjukat.
* */
public abstract class BasePage extends WebPage {
    public BasePage() {
        /*
        * Itt azt adjuk meg, hogy a page1 linkre kattintva melyik pagera navigáljon át mindet.
        * */
        add(new BookmarkablePageLink("page1", ExtendsFromBasePage1.class));
        add(new BookmarkablePageLink("page2", ExtendsFromBasePage2.class));
        add(new Label("footer", "This is in the footer"));
    }
}