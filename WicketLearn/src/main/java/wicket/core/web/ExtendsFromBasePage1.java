package wicket.core.web;

import org.apache.wicket.markup.html.basic.Label;

public class ExtendsFromBasePage1 extends BasePage{
    public ExtendsFromBasePage1(){
        add(new Label("label1", "This is in the subclass Page1"));
    }
}
