package wicket.core.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;

public class NotAjaxCounterPage extends WebPage {
/*A href link mindig újra tölti az oldalt, az pedig a modelen belül mindig növeli a változó értékét, mert a getObject() mindig
* meghívódik, hiszen a lebel használja annak értékét. Ennek az a hátránya, hogy az oldal teljes újratöltődésével jár.
* Az onClick() nincs használva, a href alapból navigál arra a linkre, ami megvan adva neki(jelen esetben magára #).*/

    public NotAjaxCounterPage(){
        Model<Integer> model = new Model<Integer>() {
            private int counter = 0;
            public Integer getObject() {
                return counter++;
            }
        };
        add(new Link("link") {
            public void onClick() {
                /*
                * A linkre kattintva nem csinálunk semmit, csak újra töltjük a paget, ami az anchor tag tulajdonságából adódik
                * */
            }
        });
        add(new Label("counter", model));
    }

}
