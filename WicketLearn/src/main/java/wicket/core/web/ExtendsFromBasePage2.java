package wicket.core.web;

import org.apache.wicket.markup.html.basic.Label;

public class ExtendsFromBasePage2 extends BasePage {
    public ExtendsFromBasePage2(){
        add(new Label("label2", "This is in the subclass Page1"));
    }
}
