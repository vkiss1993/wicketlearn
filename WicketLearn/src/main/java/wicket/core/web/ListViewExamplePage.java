package wicket.core.web;

import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListViewExamplePage extends BasePage {

    private int count = 0;

    public ListViewExamplePage() {
        final Model<Integer> model = new Model<>();
        model.setObject(count);

        final Label label = new Label("counter", model);

        label.setOutputMarkupId(true);

        /**
         * <button></button> html elemre AjaxLink Objektumot hozunk létre, ezt adjuk hozzá a pagehez, így ez a gomb
         * Ajaxos lesz.
         * */
        AjaxLink btnClose = new AjaxLink("counterButton") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                model.setObject(count++);
                target.add(label);
            }
        };

        add(btnClose);
        add(label);

        List<Pair<String, Class<? extends Page>>> data = Stream.of(
                new Pair<String, Class<? extends Page>>("ComplexExamplePage.test", AjaxCounterPage.class),
                new Pair<String, Class<? extends Page>>("ComplexExamplePage.other", HelloWorld.class),
                new Pair<String, Class<? extends Page>>("ComplexExamplePage.asd", GuestBook.class),
                new Pair<String, Class<? extends Page>>("ComplexExamplePage.qwe", AjaxCounterPage.class)
        )
                .map(pair -> new Pair<String, Class<? extends Page>>(new ResourceModel(pair.getKey()).getObject(), pair.getValue()))
                .sorted(Comparator.comparing(Pair::getKey))
                .collect(Collectors.toList());

        ListView<Pair<String, Class<? extends Page>>> repeatingView = new ListView<Pair<String, Class<? extends Page>>>("cockpitList", data) {
            @Override
            protected void populateItem(ListItem<Pair<String, Class<? extends Page>>> listItem) {
                listItem.add(new MenuItemPanel("inner", listItem.getModel()));
            }
        };

        add(repeatingView);
    }
}
